## Foreword

Hello! This is an example student blog for the [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

## About me

<!-- ![](images/avatar-photo.jpg) -->

Hello, I am Vlad. I study maths at ULB

## About me

I enjoy programming; some of my favorite projects that i've worked on are on the intersection of web development and electronics, such as creating gui interfaces in the browser for controlling and interacting with microcontrollers.

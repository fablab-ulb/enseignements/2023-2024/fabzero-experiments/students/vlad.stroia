# 2. Conception Assistée par Ordinateur (CAO)

This week we worked on creating the 3d model that we'll be printing for the next module.

## OpenSCAD

I used OpenSCAD to create my 3d model. It was the first time using this software, but i would say that it's interface it's quite intuitive.

### Parameters

```
//ANCHOR parameters
UNITS = 7;
UNIT_WIDTH = 7.8;
UNIT_HEIGHT = 9.6/2;
HOLE_RADIUS = 2.2;
//BEAM parameters
BEAM_WIDTH = UNIT_HEIGHT/3;
BEAM_LENGTH = 50;
BEAM_SPACING = 50;
```

We aim to make our models parametrically. This means that we can at any point change constants that dictate our model's dimensions.
This is helpful as it permits easily iterating our designs such that they best comply with the physical limitations of our manufacturing processes.

### Tolerances
```
error=0.01;
```
It is important to understand the tolerances that a specific manufacturing process has. This will help us design our parts such that they fit together as intended. For this we define an `error` parameter that we can change to adjust the tolerances of our model.

```
$fn = 100;
```

The `$fn` parameter defines the number of sides that a cylinder will have. This is important as it will affect the smoothness of our model. A higher value will result in a smoother model, but will also increase the time it takes to render the model.



### Modules

We take advantage of `OpenSCAD's` module system to create reusable components of our model. This will help us keep our code clean and easy to read.

```
module addHoles(units, unit_width, unit_height, hole_radius, startingY) {
    for (i = [0:units-1]){
            translate([unit_width*i, startingY, 0])cylinder(h=unit_height+error, r=hole_radius+(i*0.1), center=[0,0,unit_height/2-error/2]);
    }
};
module addMainPiece (units, unit_width, unit_height, hole_radius, startingY) {
    hull(){
        for (i = [0:units-1]){
            translate([unit_width*i, startingY, 0])cylinder(h=unit_height, r=unit_width/2, center=[0,0,unit_height/2]);
        }
    }
}
module addbeam(width, z, startingX, startingY, endX, endY){
    len = sqrt(pow(endX-startingX,2)+pow(endY-startingY,2));
    angle = atan2(endY-startingY, endX-startingX);
    translate([startingX,startingY,z])rotate(angle)cube([len, width, width]);
};
```

### Finally, we put it all together

```
difference(){
    union(){
        addMainPiece(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS, 0);
        addMainPiece(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS, BEAM_SPACING);
        addbeam(BEAM_WIDTH, 0, 0, 0, BEAM_SPACING, BEAM_SPACING);
        addbeam(BEAM_WIDTH, UNIT_HEIGHT - BEAM_WIDTH, BEAM_SPACING, 0, 0, BEAM_SPACING);
    }
    addHoles(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS, 0);
    addHoles(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS, BEAM_SPACING);
}
```

![](images/openscad.png)
